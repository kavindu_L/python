"""
center align all lines in the given file

"""

open_file=open('foo.txt','r')
file_lines=open_file.readlines()
maximum_length=len(max(file_lines))

for i in file_lines:
	print " "*((maximum_length-len(i))/2)+ i.strip()

f.close()
