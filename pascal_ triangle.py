numlines=int(raw_input("Enter number of lines : "))

if numlines==1 :
    print "1"

if numlines< 0 :
    print("Not define for negative integers !")

if numlines> 2:
    print " "*(numlines-1),"1"
    numlist=[0,1,0]
    newlist=[]
    for n in range(numlines-1):
        for i in range(len(numlist)-1):
            tot=numlist[i]+numlist[i+1]
            newlist.append(tot)
        print (numlines-2)*" "," ".join(str(i) for i in newlist)
        numlines=numlines-1
        numlist=newlist
        numlist.append(0)
        numlist.insert(0,0)
        newlist=[]
